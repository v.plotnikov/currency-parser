package com.parser.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.parser.app.data.Currency
import com.parser.app.R

class CurrencyAdapter(private var currencies: List<Currency>) :
    RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {

    private lateinit var onClickListener: OnClickListener

    fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    interface OnClickListener {
        fun onClick(currency: Currency)
    }

    fun updateCurrencies(currencies: List<Currency>) {
        this.currencies = currencies
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val root: LinearLayout = view.findViewById(R.id.currency)
        val charCode: TextView = view.findViewById(R.id.currency_char_code)
        val currentValue: TextView = view.findViewById(R.id.currency_current_value)
        val name: TextView = view.findViewById(R.id.currency_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.currency, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.root.setOnClickListener {
            onClickListener.onClick(currencies[position])
        }
        holder.charCode.text = currencies[position].CharCode
        holder.currentValue.text =
            "%.4f".format(currencies[position].Value.toDouble() / currencies[position].Nominal)
        holder.name.text = currencies[position].Name
    }

    override fun getItemCount() = currencies.size
}