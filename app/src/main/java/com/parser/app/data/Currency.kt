package com.parser.app.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currencies")
data class Currency(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val ID: String,

    @ColumnInfo(name = "num_code")
    val NumCode: Int,

    @ColumnInfo(name = "short_name")
    val CharCode: String,

    @ColumnInfo(name = "name")
    val Name: String,

    @ColumnInfo(name = "nominal")
    val Nominal: Int,

    @ColumnInfo(name = "value")
    val Value: String,

    @ColumnInfo(name = "previous_value")
    val Previous: String
)
