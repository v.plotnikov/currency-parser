package com.parser.app.data

import com.google.gson.JsonObject

data class Request(
    val Date: String,
    val PreviousDate: String,
    val PreviousURL: String,
    val Timestamp: String,
    val Valute: JsonObject
)
