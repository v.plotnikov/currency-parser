package com.parser.app.data

import androidx.room.*

@Dao
interface CurrencyDao {
    @Query("SELECT * FROM currencies")
    fun getAll(): List<Currency>

    @Insert
    fun insert(currency: Currency)

    @Update
    fun update(currency: Currency)

    @Delete
    fun delete(currency: Currency)
}