package com.parser.app

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.google.gson.Gson
import com.parser.app.adapters.CurrencyAdapter
import com.parser.app.api.Api
import com.parser.app.data.Currency
import com.parser.app.data.CurrencyDatabase
import com.parser.app.data.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class StartActivity : AppCompatActivity() {
    private val baseUrl = "https://www.cbr-xml-daily.ru/"
    private val dbName = "common"

    private lateinit var handler: Handler
    private lateinit var runnable: Runnable

    private val updateEvery = 60000L

    private lateinit var currencyList: RecyclerView
    private lateinit var db: CurrencyDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        currencyList = findViewById(R.id.currency_list)
        currencyList.layoutManager = LinearLayoutManager(this)

        db = Room.databaseBuilder(applicationContext, CurrencyDatabase::class.java, dbName)
            .allowMainThreadQueries().build()

        if (!getDatabasePath(dbName).exists()) {
            getCurrencies()
        } else {
            setAdapter(db.currencyDao().getAll())
        }

        handler = Handler(Looper.getMainLooper())
        runnable = object : Runnable {
            override fun run() {
                getCurrencies()
                Toast.makeText(this@StartActivity, "Список валют был обновлен", Toast.LENGTH_SHORT)
                    .show()
                handler.postDelayed(this, updateEvery)
            }
        }
        handler.postDelayed(runnable, updateEvery)
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(runnable)
    }

    private fun getCurrencies() {
        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        retrofit.create(Api::class.java).getCurrencies().enqueue(object : Callback<Request> {
            override fun onResponse(call: Call<Request>, response: Response<Request>) {
                val request = response.body() as Request
                val currencies: MutableList<Currency> = mutableListOf()
                for (i in request.Valute.entrySet()) {
                    currencies.add(Gson().fromJson(i.value, Currency::class.java))
                }
                val isExist = getDatabasePath(dbName).exists()
                for (i in currencies) {
                    if (isExist) {
                        db.currencyDao().update(i)
                    } else {
                        db.currencyDao().insert(i)
                    }
                }
                setAdapter(currencies)
            }

            override fun onFailure(call: Call<Request>, t: Throwable) {
                Toast.makeText(
                    this@StartActivity,
                    "Не удалось получить список валют",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    fun setAdapter(currencies: List<Currency>) {
        if (currencyList.adapter == null) {
            currencyList.adapter = CurrencyAdapter(currencies)

            (currencyList.adapter as CurrencyAdapter).setOnClickListener(object :
                CurrencyAdapter.OnClickListener {
                override fun onClick(currency: Currency) {
                    showDialog(currency)
                }
            })
        } else {
            (currencyList.adapter as CurrencyAdapter).updateCurrencies(currencies)
            for (i in 0..currencies.size) {
                currencyList.adapter!!.notifyItemChanged(i)
            }
        }
    }

    fun showDialog(currency: Currency) {
        val builder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater

        val root = inflater.inflate(R.layout.dialog, null)

        builder.setView(root)
            .setTitle("Перевести в ${currency.CharCode}")
            .setMessage("Введите значение в российских рублях")
            .setPositiveButton("ОК") { _, _ ->
                val value = root.findViewById<TextView>(R.id.value).text.toString().toDouble()
                Toast.makeText(
                    this,
                    "%.4f ${currency.CharCode}".format(value / (currency.Value.toDouble() / currency.Nominal)),
                    Toast.LENGTH_LONG
                ).show()
            }
            .setNegativeButton("Отмена") { dialog, _ ->
                dialog?.cancel()
            }
        builder.show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_update) {
            getCurrencies()
        }
        return super.onOptionsItemSelected(item)
    }
}