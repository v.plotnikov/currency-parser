package com.parser.app.api

import com.parser.app.data.Request
import retrofit2.Call
import retrofit2.http.GET

interface Api {
    @GET("daily_json.js")
    fun getCurrencies(): Call<Request>
}